(function ($){
    var fadeTime = 600;
    $.isScroll = false;
	$.fn.MainDataInIt = function (){
        $.Body.ClickInIt();
        $.Body.ScrollEvent();
        $('.home-banner-row_inner').slick({
            centerMode: true,
            variableWidth: true,
            slidesToShow: 1,
            autoplay: false,
            autoplaySpeed: 10000,
            prevArrow: '.ez-slide_prev',
            nextArrow: '.ez-slide_next',
            arrows: true,
            dots: true,
            rows: 0, // Fix vor v1.8.0-1
            dotsClass: 'custom-dots', //slick generates this <ul.custom-dots> within the appendDots container
            customPaging:   function (slider, i) {
                                // console.log(slider);
                                var slideNumber = (i + 1),
                                    totalSlides = slider.slideCount;
                                return '<a class="dot" role="button" title="' + slideNumber + ' of ' + totalSlides + '"><span class="string">' + slideNumber + '/' + totalSlides + '</span></a>';
                            },
            responsive: [{
                breakpoint: 575.9,
                settings: {
                    centerMode: false,
                    variableWidth: false,
                }
            }]
        });
        $('.sub-banner-row_inner').slick({
            // variableWidth: true,
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 10000,
            prevArrow: '.ez-slide_prev',
            nextArrow: '.ez-slide_next',
            arrows: true,
            dots: true,
            rows: 0, // Fix vor v1.8.0-1
            dotsClass: 'custom-dots', //slick generates this <ul.custom-dots> within the appendDots container
            customPaging:   function (slider, i) {
                                // console.log(slider);
                                var slideNumber = (i + 1),
                                    totalSlides = slider.slideCount;
                                return '<a class="dot" role="button" title="' + slideNumber + ' of ' + totalSlides + '"><span class="string">' + slideNumber + '/' + totalSlides + '</span></a>';
                            },
            responsive: [{
                breakpoint: 575.9,
                settings: {
                    centerMode: false,
                    variableWidth: false,
                }
            }]
        });
        $('.member-in-ez-card').slick({
            centerMode: true,
            // variableWidth: true,
            mobileFirst: true,
            slidesToShow: 1,
            autoplay: false,
            prevArrow: '.ez-slide_prev',
            nextArrow: '.ez-slide_next',
            arrows: true,
            dots: true,
            rows: 0, // Fix vor v1.8.0-1
            dotsClass: 'custom-dots', //slick generates this <ul.custom-dots> within the appendDots container
            customPaging:   function (slider, i) {
                                // console.log(slider);
                                var slideNumber = (i + 1),
                                    totalSlides = slider.slideCount;
                                return '<a class="dot" role="button" title="' + slideNumber + ' of ' + totalSlides + '"><span class="string">' + slideNumber + '/' + totalSlides + '</span></a>';
                            },
            responsive: [{
                breakpoint: 575.98,
                settings: 'unslick'
            }]
        });
        $('.article-in-ez-card').slick({
            // variableWidth: true,
            slidesToShow: 3,
            autoplay: false,
            autoplaySpeed: 10000,
            prevArrow: '.ez-slide-sub_prev',
            nextArrow: '.ez-slide-sub_next',
            arrows: true,
            dots: true,
            rows: 0, // Fix vor v1.8.0-1
            dotsClass: 'custom-dots', //slick generates this <ul.custom-dots> within the appendDots container
            customPaging:   function (slider, i) {
                                // console.log(slider);
                                var slideNumber = (i + 1),
                                    totalSlides = slider.slideCount;
                                return '<a class="dot" role="button" title="' + slideNumber + ' of ' + totalSlides + '"><span class="string">' + slideNumber + '/' + totalSlides + '</span></a>';
                            },
            responsive: [{
                breakpoint: 575.9,
                settings: {
                    slidesToShow: 1,
                    centerMode: false,
                    variableWidth: false,
                }
            }]
        });
        $('.video-sp-ez-card').slick({
            centerMode: true,
            variableWidth: true,
            slidesToShow: 1,
            autoplay: false,
            autoplaySpeed: 10000,
            prevArrow: '.ez-slide-sub_prev',
            nextArrow: '.ez-slide-sub_next',
            arrows: true,
            dots: true,
            rows: 0, // Fix vor v1.8.0-1
            dotsClass: 'custom-dots', //slick generates this <ul.custom-dots> within the appendDots container
            customPaging:   function (slider, i) {
                                // console.log(slider);
                                var slideNumber = (i + 1),
                                    totalSlides = slider.slideCount;
                                return '<a class="dot" role="button" title="' + slideNumber + ' of ' + totalSlides + '"><span class="string">' + slideNumber + '/' + totalSlides + '</span></a>';
                            },
            responsive: [{
                breakpoint: 575.9,
                settings: {
                    slidesToShow: 1,
                    centerMode: false,
                    variableWidth: false,
                }
            }]
        });
        $('.album-sp-ez-card').slick({
            // variableWidth: false,
            slidesToShow: 2,
            autoplay: false,
            autoplaySpeed: 10000,
            prevArrow: '.ez-slide-sub_prev',
            nextArrow: '.ez-slide-sub_next',
            arrows: true,
            dots: true,
            rows: 0, // Fix vor v1.8.0-1
            dotsClass: 'custom-dots', //slick generates this <ul.custom-dots> within the appendDots container
            customPaging:   function (slider, i) {
                                // console.log(slider);
                                var slideNumber = (i + 1),
                                    totalSlides = slider.slideCount;
                                return '<a class="dot" role="button" title="' + slideNumber + ' of ' + totalSlides + '"><span class="string">' + slideNumber + '/' + totalSlides + '</span></a>';
                            },
            responsive: [{
                breakpoint: 991.9,
                settings: {
                    slidesToShow: 1,
                    centerMode: false,
                    variableWidth: false,
                }
            }]
        });
		$.Body.ResizeInIt();
        $.Body.CustomSelect();
    };
//------------------------ click function ------------------------
    $.fn.ClickInIt = function (){
        $.Body.on('click','.btn_totop, .ez-hamburger, .ez-collapse_btn, .btn_open-sidebar-m, .btn_open-sidebar, .btn_close-sidebar, .js-nav_btn-signin, .thumbnail, .js-btn-tochapter', function(e){
            $.temp_click = $(this);
            switch (true){
                // case $.temp_click.attr('id') == 'copyBtn':
                //     break;
                // @mobileNav-list
                case $.temp_click.hasClass('ez-hamburger'):
                    $('.ez-nav').toggleClass('nav_togglebg');
                    $('.wrap-content').toggleClass('clicked');
                    console.log('hamburger')
                    break;
                // @mobileNav
                case $.temp_click.hasClass('js-nav_btn-signin'):
                    // if (!$('.ez-nav').hasClass("nav_togglebg")) {
                    //     $('.ez-nav').toggleClass('nav_togglebg');
                    // }
                    console.log('member');
                    break;
                // @回到頂
                case $.temp_click.hasClass('btn_totop'):
                    console.log('!!');
                    $('html,body').animate({scrollTop: 0});
                    break;
                //@折疊重置網頁高度
                case $.temp_click.hasClass('ez-collapse_btn'):
                    // $.Window.trigger('resize', function(){
                    //     console.log('secFooterPos:' + $.secFooterPos);
                    // });
                    console.log('secFooterPos:' + $.secFooterPos);
                    console.log($.b_h);
                    console.log('ez-collapse_btn');
                    break;
                //@course側邊欄_pc_開
                case $.temp_click.hasClass('btn_open-sidebar'):
                    $('.course-player_sidebar').removeClass('d-none');
                    $('.course-player-content, .ez-course-player-nav').addClass('sidebar-open');
                    break;
                //@course側邊欄_m_開
                case $.temp_click.hasClass('btn_open-sidebar-m'):
                    $('.course-player_sidebar').removeClass('d-none');
                    break;
                //@course側邊欄_關
                case $.temp_click.hasClass('btn_close-sidebar'):
                    $('.course-player_sidebar').addClass('d-none');
                    $('.course-player-content, .ez-course-player-nav').removeClass('sidebar-open');
                    console.log('close')
                    break;
                //@video影片預覽
                case $.temp_click.hasClass('thumbnail'):
                    var thisYoutubeId = $(this).attr('data-youtube-id');
        
                    var video = '<iframe id="video'+ thisYoutubeId+ '" src="'+ $(this).attr('data-video')+'" webkitAllowFullScreen mozallowfullscreen allowFullScreen frameBorder="0" data-ytid="'+thisYoutubeId+'"></iframe>';
                
                    $(this).replaceWith(video);
                    break;
                // @看課程單元
                case $.temp_click.hasClass('js-btn-tochapter'):
                    console.log('!!');
                    $('html,body').animate({scrollTop: $('.course-info_chapter-list_ttl').offset().top});
                    // $('html,body').animate({scrollTop: 0});
                    break;
            }
        });
    }
//------------------------ scroll function ------------------------
    $.fn.ScrollEvent = function(){
        $(window).on('scroll', function(){
            var scrollTop = document.documentElement.scrollTop || document.body.scrollTop || 0;
            // console.log('scrollTop' + scrollTop);
            $.Window.trigger('resize');
            // @ez-nav切換
            if(scrollTop == 0){
                // console.log($.isScroll)
                $('.ez-nav').removeClass('nav_scrollbg');
                $('.catagory-pc').addClass('d-none');
                $('.ez-nav_sub').collapse('hide');
                $('.ez-nav-search_bg').css('background-color','#FFF')
                // $.isScroll = true;
            }else if(scrollTop !== 0){
                // console.log($.isScroll)
                $('.ez-nav').addClass('nav_scrollbg');
                $('.catagory-pc').removeClass('d-none');
                $('.ez-nav-search_bg').css('background-color','#F2F4FF')
                // $.isScroll = true;
            }
            // @ez-player切換
            // console.log('scroll:' + (scrollTop + $.w_h));
            // console.log('secFooterPos:' + $.secFooterPos);
            // console.log('player' + $.player_h);
            // console.log('playerBox' + $.playerBox_h);
            if ((scrollTop + $.w_h) >= $.secFooterPos) {
				$('.ez-player').removeClass('fixed_bottom');
			} else {
				$('.ez-player').addClass('fixed_bottom');
			}
            // @ez-course-sidebar切換
        });
    }
//------------------------ Resize function ------------------------
	$.fn.ResizeInIt = function (){
        function resizing(){
            $.b_w = $.Body.width();
			$.b_h = $.Body.height();
			$.w_w = $.Window.width();
			$.w_h = $.Window.height();
			$.d_h = $(document).height();
            $.secFooterPos = $('.page-footer').position().top;
            $.player_h = $('.ez-player').height();
            $('.ez-player_box').css('height' ,$.player_h + 'px');

            if($.w_w < 768){
                // console.log('mobile')
                $('.ez-nav_sub').collapse('hide');
                $('.course-player-content, .ez-course-player-nav').removeClass('sidebar-open');
            }else{
                // console.log('pc')
                $('.ez-nav_mobile-list').collapse('hide');
                $('.ez-nav').removeClass('nav_togglebg');
                $('.wrap-content').removeClass('clicked');
            }
            $('.member-in-ez-card').slick('resize');
        }
        $.Window.resize(resizing).trigger('resize');
	};
//------------------------ Custom Select --------------------------
    $.fn.CustomSelect = function (){
        var x, i, j, l, ll, selElmnt, a, b, c;
        /* Look for any elements with the class "custom-select": */
        x = document.getElementsByClassName("custom-select");
        l = x.length;

        for (i = 0; i < l; i++) {
            selElmnt = x[i].getElementsByTagName("select")[0];
            ll = selElmnt.length;

            /* For each element, create a new DIV that will act as the selected item: */
            a = document.createElement("DIV");
            a.setAttribute("class", "select-selected");
            a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
            x[i].appendChild(a);

            /* For each element, create a new DIV that will contain the option list: */
            b = document.createElement("DIV");
            b.setAttribute("class", "select-items select-hide");

            for (j = 1; j < ll; j++) {
                /* For each option in the original select element,
                create a new DIV that will act as an option item: */
                c = document.createElement("DIV");
                c.innerHTML = selElmnt.options[j].innerHTML;
                c.addEventListener("click", function(e) {
                    /* When an item is clicked, update the original select box,
                    and the selected item: */
                    var y, i, k, s, h, sl, yl;
                    s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                    sl = s.length;
                    h = this.parentNode.previousSibling;
                    for (i = 0; i < sl; i++) {
                        if (s.options[i].innerHTML == this.innerHTML) {
                            s.selectedIndex = i;
                            h.innerHTML = this.innerHTML;
                            y = this.parentNode.getElementsByClassName("same-as-selected");
                            yl = y.length;
                            for (k = 0; k < yl; k++) {
                                y[k].removeAttribute("class");
                            }
                            this.setAttribute("class", "same-as-selected");
                            break;
                        }
                    }
                    h.click();
                });
                b.appendChild(c);
            }

            x[i].appendChild(b);

            a.addEventListener("click", function(e) {
                /* When the select box is clicked, close any other select boxes,
                and open/close the current select box: */
                e.stopPropagation();
                closeAllSelect(this);
                this.nextSibling.classList.toggle("select-hide");
                this.classList.toggle("select-arrow-active");
            });
        }

        function closeAllSelect(elmnt) {
            /* A function that will close all select boxes in the document,
            except the current select box: */
            var x, y, i, xl, yl, arrNo = [];
            x = document.getElementsByClassName("select-items");
            y = document.getElementsByClassName("select-selected");
            xl = x.length;
            yl = y.length;
            for (i = 0; i < yl; i++) {
                if (elmnt == y[i]) {
                    arrNo.push(i)
                } else {
                    y[i].classList.remove("select-arrow-active");
                }
            }
            for (i = 0; i < xl; i++) {
                if (arrNo.indexOf(i)) {
                    x[i].classList.add("select-hide");
                }
            }
        }

        /* If the user clicks anywhere outside the select box,
        then close all select boxes: */
        document.addEventListener("click", closeAllSelect);
    };
//------------------------ Stop Video --------------------------
    $("#openvideo").on('hidden.bs.modal', function (e) {
        $("#openvideo iframe").attr("src", $("#openvideo iframe").attr("src"));
    });
})(jQuery);

$(function(){
    $.html =$('html');
    $.Body =$('body');
    $.Window = $(window);
    $.Loading = $.Body.find('div#loading');
    $(document).ready(function(){
            $.Body.MainDataInIt();
            console.log('load');
        }
    );
});