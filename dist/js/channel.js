(function ($){
    var fadeTime = 600;
    




	$.fn.MainDataInIt = function (){
        $('.home-banner-row_inner').slick({
            centerMode: true,
            variableWidth: true,
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            prevArrow: '.ez-slide_prev',
            nextArrow: '.ez-slide_next',
            arrows: true,
            dots: true,
            rows: 0, // Fix vor v1.8.0-1
            dotsClass: 'custom-dots', //slick generates this <ul.custom-dots> within the appendDots container
            customPaging:   function (slider, i) {
                                // console.log(slider);
                                var slideNumber = (i + 1),
                                    totalSlides = slider.slideCount;
                                return '<a class="dot" role="button" title="' + slideNumber + ' of ' + totalSlides + '"><span class="string">' + slideNumber + '/' + totalSlides + '</span></a>';
                            },
            responsive: [{
                breakpoint: 575.9,
                settings: {
                    centerMode: false,
                    variableWidth: false,
                }
            }]
        });
    };
})(jQuery);

$(function(){
    $.html =$('html');
    $.Body =$('body');
    $.Window = $(window);
    $.Loading = $.Body.find('div#loading');
    $.anibox = $.Body.find('#anibox')
    $(document).ready(function(){
            $.Body.MainDataInIt();
            console.log('load');
        }
    );
});